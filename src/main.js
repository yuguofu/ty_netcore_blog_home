import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/css/style.css'
import './common/font/font.css'

// 引入ant-design-vue
import Antd, { message } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
Vue.use(Antd);

// 引入element-ui
import { Button, Badge } from 'element-ui';
Vue.use(Button)
Vue.use(Badge)

// layui样式
import '../node_modules/layui-src/dist/css/layui.css'


// idea.css  atom-one-light.css github-dark.css
import hljs from 'highlight.js';
// import 'highlight.js/styles/default.css'
// import 'highlight.js/styles/monokai-sublime.css'
import 'highlight.js/styles/atom-one-dark.css'
// import 'highlight.js/styles/github.css'


import axios from 'axios'
// 配置axios
axios.defaults.baseURL = "http://localhost:5000/api/";
axios.defaults.headers['Content-Type'] = 'application/json;charset=UTF-8';  //配置请求头的内容协商
Vue.prototype.$axios = axios;   //axios挂接到vue

// 配置消息弹框
Vue.prototype.$message = message;


Vue.config.productionTip = false


//自定义一个代码高亮指令
// Vue.directive('highlight', function (el) {
//     let blocks = el.querySelectorAll('pre code');
//     setTimeout(() => {
//         blocks.forEach((block) => {
//             hljs.highlightBlock(block)
//         })
//     }, 200);
// });

//自定义一个代码高亮指令
Vue.directive('highlight', function (el) {
    let highlight = el.querySelectorAll('pre code');
    highlight.forEach((block) => {
        hljs.highlightBlock(block);
    });
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');




