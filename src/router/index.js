import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Index from '../components/Index.vue'
import Category from '../components/Category.vue'
import Tag from '../components/Tag.vue'
import Blog from '../components/Blog.vue'
import Archive from '../components/Archive.vue'
import About from '../components/About.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '',
                component: Index,
                meta: {
                    title: '主页',
                }
            },
            {
                path: 'cate',
                component: Category,
                meta: {
                    title: '分类',
                }
            },
            {
                path: 'tag',
                component: Tag,
                meta: {
                    title: '标签',
                }
            },
            {
                path: 'blog/:id',
                name: 'Blog',
                component: Blog,
                meta: {
                    title: '文章详情',
                }
            },
            {
                path: 'archive',
                name: 'Archive',
                component: Archive,
                meta: {
                    title: '归档',
                }
            }
            ,
            {
                path: 'about',
                name: 'About',
                component: About,
                meta: {
                    title: '关于',
                }
            }
        ]
    },

]

const router = new VueRouter({
    routes
});

// 处理网页标题
router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title ? to.meta.title : '加载中';
        next();
    }
});

export default router
