# ty_netcore_blog_home

#### 介绍
ASP.NET Core Api + Vue前后端分离博客，此为前端前台展示

#### 软件架构
vue2.6、Ant Design of Vue、Element UI、LayUI、highlight.js、typo.css
  
  
TyNetCoreBlog项目 链接：<https://gitee.com/yuguofu/ty-net-core-blog>为ASP.NET Core后端Api  

ty_netcore_blog_admin项目 链接：<https://gitee.com/yuguofu/ty_netcore_blog_admin> 为vue前端后台管理  

ty_netcore_blog_home项目 链接：<https://gitee.com/yuguofu/ty_netcore_blog_home> 为vue前端前台展示
 
 
#### 安装教程

1.  visual studio 2019打开TyNetCoreBlog项目编译运行
2.  ty_netcore_blog_admin克隆到本地，npm install安装依赖，npm run serve运行调试、npm run build打包发布，浏览器 <http://localhost:8088/> 查看后台管理
3.  ty_netcore_blog_home克隆到本地，npm install安装依赖，npm run serve运行调试、npm run build打包发布，浏览器 <http://localhost:8087/> 查看前台展示

#### 使用说明

1.  visual studio 2019打开TyNetCoreBlog项目编译运行
2.  ty_netcore_blog_admin克隆到本地，npm install安装依赖，npm run serve运行调试、npm run build打包发布，浏览器 <http://localhost:8088/> 查看后台管理
3.  ty_netcore_blog_home克隆到本地，npm install安装依赖，npm run serve运行调试、npm run build打包发布，浏览器 <http://localhost:8087/> 查看前台展示

#### 预览
![首页](https://images.gitee.com/uploads/images/2020/1201/213030_355ebcac_5348320.png "屏幕截图.png")  
![分类](https://images.gitee.com/uploads/images/2020/1201/213104_a33676f2_5348320.png "屏幕截图.png")  
![标签](https://images.gitee.com/uploads/images/2020/1201/213129_68e28cf7_5348320.png "屏幕截图.png")  
![归档](https://images.gitee.com/uploads/images/2020/1201/213148_5ce5500d_5348320.png "屏幕截图.png")  
![关于](https://images.gitee.com/uploads/images/2020/1201/213211_93d36160_5348320.png "屏幕截图.png")
![文章详情](https://images.gitee.com/uploads/images/2020/1201/213336_7eff3440_5348320.png "屏幕截图.png")